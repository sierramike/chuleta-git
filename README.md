# Chuleta Git

## Básicos

| Acción    | Comando |    comentario|   
| --- | --- | --- |
| **Ayuda >>>>>>>** |```git command --help```|
| Inicializar (crear) un nuevo repo en local| ```git init```| |
| Clonar repositorio remoto | ```git clone <url>```|
| Añadir ```archivo``` al stage | ```git add archivo```|
| Añadir todo al stage | ```git add .``` | Todo excepto lo ignorado con ```.gitignore```|
| Hacer **commit** con "comentario" | ```git commit -m "comentario"``` |
| Hacer **push** (enviar) a remoto | ```git push origin master```|
| Mostra estado|```git status```|
| Mostrar historia | ```git log```|
|**Ramas**|
| Crear una ```nueva-rama``` y saltar a ella | ```git checkout -b nueva-rama```|
| Saltar a una ```rama``` ya existente | ```git checkout rama```|
| Push a ```rama``` remota | ```git push origin rama```|

## Trabajo con remotos
| Acción    | Comando |    comentario|   
| --- | --- | --- |
| Clonar en local un repo remoto | ```git clone https://gitlab.com/sierramike/chuleta-git.git``` | crea ```/chuleta-git``` y añade este repo|
| Actualizar local con el remoto | ```git pull```|
| Actualizar remoto con el local| ```git push origin master``` | ```master``` o la rama que queramos|
| Mostrar url del remoto | ```git remote -v``` o ```git remote --verbose``` |
| Desconectar de un remoto | ```git remote remove <name>``` o ```git remote remove origin```|



## Links sobre Git
- [Think Like (a) Git](http://think-like-a-git.net/)
- [git - la guía sencilla](https://rogerdudler.github.io/git-guide/index.es.html)